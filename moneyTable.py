import tkinter as tk
from tkinter import ttk
import miscAdditions as ma



def createElementList(list1:list,list2:list):
    elementListC = []
    for i in range(len(list1)):
        tempList = []
        tempList.append(list1[i])
        tempList.append(str(list2[i])+" €")
        elementListC.append(tempList)
    elementListC.append(["TOTAL",""])
    return elementListC


def moneyTable(window,categoryList:list,quantityList:list):
    if len(categoryList) != len(quantityList):
        raise ValueError("The list:",categoryList,"haven't the same length than the list:",quantityList)
    global elementList
    elementList = createElementList(categoryList,quantityList)
    frame = tk.LabelFrame(window)
    frame.grid(padx=30,pady = 10,sticky=tk.W,ipadx = 300,ipady=350, row=0,column=0)
    frame.config(background=(ma.rgb_to_hex(100, 100, 100)),bd=3)
    global tree
    tree = ttk.Treeview(frame)
    column_list = ["Category", "Money"]
    tree['columns'] = column_list
    tree["show"] = "headings"
    for column in column_list:
        tree.heading(column, text=column)
        max = 0
        for i in range(len(categoryList)):
            if len(categoryList[i]) > max:
                max = len(categoryList[i])
        if column == column_list[0]:
            tree.column(column, width=max*8)
        else:
            tree.column(column, width=90)
    tree.place(relheight=1, relwidth=1)
    treescrollY = tk.Scrollbar(frame)
    treescrollY.configure(command=tree.yview)
    tree.configure(yscrollcommand=treescrollY.set)
    treescrollY.pack(side="right", fill="y")
    treescrollX = tk.Scrollbar(frame, orient=tk.HORIZONTAL)
    treescrollX.configure(command=tree.xview)
    tree.configure(xscrollcommand=treescrollX.set)
    treescrollX.pack(side="bottom", fill="x")
    def handle_click(event):
        if tree.identify_region(event.x, event.y) == "separator":
            return "break"
    tree.bind('<Button-1>', handle_click)
    reload()
    return tree


def reload():
    total = 0
    for i in range(len(elementList)-1):

        elementToModify = elementList[i][1]
        elementToModify = elementToModify.replace(" €","")
        total += eval(elementToModify)
    elementList[-1][1] = str(total) + " €"
    tree.delete(*tree.get_children())
    for row in elementList:
        tree.insert("", "end", values=row)

def changeValue(key,value):
    for i in range(len(elementList)):
        if elementList[i][0] == key:
            elementList[i][1] = str(value) + " €"
            break
    reload()

def addElement(*elements):
    for element in elements:
        element[1] = str(element[1]) + " €"
        elementList.insert(len(elementList)-1,element)
    reload()

def removeElement(element):
    for i in range(len(elementList)):
        if elementList[i][0] == element:
            elementList.remove(elementList[i])
            break
    reload()

