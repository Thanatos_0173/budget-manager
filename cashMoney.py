import tkinter as tk


def cashWidget(window,value,row,column,stick,padx):
    global widget
    text = "Cash: " + str(value) + " €"
    widget = tk.Label(window,text=text,anchor=tk.W,justify=tk.LEFT,width = 10 * len(text))
    widget.grid(row=row,column=column,sticky=stick,padx=padx,columnspan=3)


def returnValue():
    return float((widget.cget("text").replace(" €","")).replace("Cash: ",""))


def addMoney(value):
    current = returnValue()
    widget.config(text="Cash: "+str(current + value) + " €")

def removeMoney(value):
    addMoney(-value)


