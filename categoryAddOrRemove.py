import textBoxManager as tbm

def removeElement(list1,list2,elem1,elem2):
    list1.remove(elem1)
    list2.remove(elem2)
    tbm.addMessage('>> Sucessfully removed "' + elem1 + '" category')
    return list1,list2
def addElement(list1,list2,defaultMoneyList,categoryName,moneyValue):
     try:
         elem2float = float(moneyValue)
         list1.append(categoryName)
         list2.append(elem2float)
         defaultMoneyList.append(elem2float);
         tbm.addMessage('>> Sucessfully added "' + categoryName + '" category with base money value of ' + str(elem2float) + ' €')
     except FloatingPointError:
         tbm.addMessage('>> Can\'t convert\' ' + moneyValue + '\' to a number')
     return list1,list2,defaultMoneyList

