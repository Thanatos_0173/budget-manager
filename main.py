import moneyTable as mt
import moneyActionTable as mat
import textBoxManager as tbm
import tkinter as tk
import fileLoader as fl
import customFileSaver as cfs
import cashMoney as cm
import menuBarHandler as mbh
import moneyManager as mm

from tkinter import messagebox
from tkinter import ttk
from publicVariables import getValues

class Exist(Exception):
     pass

def removeCategory():
     window = tk.Toplevel()
     window.minsize(200,200)
     window.resizable(False,False)
     clicked = tk.StringVar()
     clicked.set(moneyClass[0])
     dropdown = tk.OptionMenu(window, clicked, *moneyClass)
     dropdown.place(x=10,y=10)
     def remove():
         window.destroy()
         mt.removeElement(clicked.get())
         moneyQuantity.remove(moneyQuantity[moneyClass.index(clicked.get())])
         moneyClass.remove(clicked.get())
         tbm.addMessage('>> Sucessfully removed "' + clicked.get() + '" category')
         if len(moneyClass) == 0:
              remove_category_button.config(state=tk.DISABLED)
              edit_money_button.config(state=tk.DISABLED)
     button = tk.Button(window,text = "Delete category", command = remove)
     button.place(x=10,y=50)

def addCategory():
     window = tk.Toplevel()
     window.minsize(200,200)
     window.resizable(False,False)
     clicked = tk.StringVar()
     clicked.set('Category Name')
     nameEntry = tk.Entry(window,textvariable=clicked)
     nameEntry.place(x=10,y=10)
     clicked2 = tk.StringVar()
     clicked2.set('Category Default Money')
     moneyEntry = tk.Entry(window,textvariable=clicked2)
     moneyEntry.place(x=10,y=40)
     def add():
          window.destroy()
          try:
               for i in moneyClass:
                    if i == clicked.get():
                         raise Exist
               mt.addElement([clicked.get(),clicked2.get()])
               tbm.addMessage('>> Sucessfully added "' + clicked.get() + '" category with base money value of ' + clicked2.get() + ' €')
               moneyClass.append(clicked.get())
               moneyQuantity.append(float(clicked2.get()))
               defaultValues.append(float(clicked2.get()))
          except ValueError:
               mt.removeElement(clicked.get())
               tbm.addMessage('>> Impossible to convert "' + clicked2.get() + '" to a number')
          except Exist:
               tbm.addMessage('>> The category "' + clicked.get() + '" already exist')
          remove_category_button.config(state='normal')
          edit_money_button.config(state='normal')

     button = tk.Button(window,text = "Add category", command = add)
     button.place(x=10,y=150)
def defaultValuesFunction():
     for i in range(len(moneyClass)):
          moneyQuantity[i] = defaultValues[i]
          mt.changeValue(moneyClass[i],defaultValues[i])
     tbm.addMessage(str(len(moneyClass)) + " have been set to their default values")



def main(dict,UUID):
     filesLoaded = fl.loadDict(dict,getValues(UUID).get('isUntitled'))
     global isFileNotEmpty
     global moneyClass
     global moneyQuantity
     global actionHappened
     global defaultValues
     moneyClass = filesLoaded[0]
     moneyQuantity = filesLoaded[1]
     actionHappened = filesLoaded[2]
     defaultValues = filesLoaded[3]
     global mainWindow
     rootWindow = tk.Tk()
     rootWindow.resizable(True, True)
     rootWindow.title(getValues(UUID).get('name') if getValues(UUID).get('name') != None else "Untitled")
     main_frame = tk.Frame(rootWindow)
     main_frame.pack(fill=tk.BOTH,expand=1)
     sec = tk.Frame(main_frame)
     sec.pack(fill=tk.BOTH,side=tk.BOTTOM)

     my_canvas = tk.Canvas(main_frame)
     my_canvas.pack(side=tk.LEFT,fill=tk.BOTH,expand=1)
     x_scrollbar = tk.Scrollbar(sec,orient=tk.HORIZONTAL,command=my_canvas.xview)
     x_scrollbar.pack(side=tk.BOTTOM,fill=tk.X)
     y_scrollbar = tk.Scrollbar(main_frame,orient=tk.VERTICAL,command=my_canvas.yview)
     y_scrollbar.pack(side=tk.RIGHT,fill=tk.Y)
     my_canvas.configure(xscrollcommand=x_scrollbar.set)
     my_canvas.configure(yscrollcommand=y_scrollbar.set)
     my_canvas.bind("<Configure>",lambda e: my_canvas.config(scrollregion= my_canvas.bbox(tk.ALL)))


     mainWindow = tk.Frame(my_canvas)
     my_canvas.create_window((0,0),window= mainWindow, anchor="nw")
     notebook = ttk.Notebook(my_canvas)
     notebook.pack(fill='both', expand=True)

     # Create tabs
     mainTab = ttk.Frame(notebook)
     tab2 = ttk.Frame(notebook)
     notebook.add(mainTab, text='Main')
     notebook.add(tab2, text='Tab 2')





     global moneyTable
     moneyTable = mt.moneyTable(mainTab,moneyClass,moneyQuantity)
     mat.moneyActionTable(mainTab,actionHappened)
     cm.cashWidget(mainTab,row=1,column=0,stick=tk.NW,padx=30,value=filesLoaded[4])
     mbh.menuButton(rootWindow,moneyClass,moneyQuantity,cm.returnValue(),actionHappened,defaultValues,UUID)
     # Buttons

     global textBox
     textBox = tbm.textBox(mainTab)
     global remove_category_button
     global add_category_button
     global edit_money_button

     remove_category_button = tk.Button(mainTab,text="Remove a category",command=removeCategory)
     add_category_button = tk.Button(mainTab,text="Add a category",command=addCategory)

     global newPath


     edit_money_button = tk.Button(mainTab,text="Manage money",command = lambda: mm.loadWindow(moneyClass,moneyQuantity))

     defaultValuesButton = tk.Button(mainTab,text = "Reset to default values", command = defaultValuesFunction)
     remove_category_button.grid(column = 0,row = 1,sticky=tk.W,padx=30,pady=30)
     add_category_button.grid(column = 0,row = 1, sticky=tk.W,padx=200,pady=30)
     edit_money_button.grid(column=1,row=1,sticky=tk.W,padx=30,pady=10)
     defaultValuesButton.grid(column = 0,row = 1, sticky=tk.W,padx=30,pady=30)

     if moneyClass == []:
          remove_category_button.config(state=tk.DISABLED)
          edit_money_button.config(state=tk.DISABLED)

     def on_closing():
          if getValues(UUID).get('isUntitled') or [moneyClass,moneyQuantity,actionHappened,cm.returnValue()] != cfs.getContent(getValues(UUID).get('path'),getValues(UUID).get('name')): #The and allow to not execute the cfh.getPath, wich lead to an error if the file is untitled.
               rootWindow.attributes('-topmost',0)
               if messagebox.askyesno('Quit','Careful! \nYou have unsaved changes. Are you sure you want to quit?'):
                    rootWindow.destroy()
          else:
               rootWindow.destroy()
     rootWindow.protocol('WM_DELETE_WINDOW',on_closing)
     rootWindow.mainloop()

