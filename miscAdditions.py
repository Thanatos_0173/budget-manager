import tkinter as tk


def rgb_to_hex(r, g, b):
    return("#"+"0"*(2-len(hex(r)[2:]))+hex(r)[2:]+"0"*(2-len(hex(g)[2:]))+hex(g)[2:]+"0"*(2-len(hex(b)[2:]))
    + hex(b)[2:])


def str_to_list(arg):
    arg = arg.split()
    return arg


def list_to_string(arg):
    strl1 = ""
    for ele in arg:
        strl1 += str(ele) + " "
    return strl1


def reloadTreeview(tree, list):
    tree.delete(*tree.get_children())
    for row in list:
        tree.insert("", "end", values=row)


def convertActualMoneyList(list, char, list1, list2):
    list.clear()
    total = 0
    for i in range(len(list)):
        tempList = []
        tempList.append(list1[i])
        tempList.append(str(list2[i])+" "+char)
        total += list2[i]
        list.append(tempList)
    list.append(["TOTAL", str(total) + " €"])
    return list


def disableAndEnabledDropdownWhenListContainOneElement(list, element, dropdown):
    if len(list) == 1 and list == [element]:
        dropdown.config(state=tk.DISABLED)
    else:
        dropdown.config(state="normal")


def dropdownReloader(screen,StringVar,list,dropdown,x,y):
    StringVar = tk.StringVar()
    StringVar.set(list[0])
    dropdown = tk.OptionMenu(screen, StringVar,*list)
    dropdown.place(x=x,y=y)

def listTransformer(list:list):
        for k in list:
            tempList = []
            for i in range(len(list)):
                if i <= 2:
                    tempList.append(k[i])
                elif i == 3:
                    tempList.append(str(k[i]+" €"))
                else:
                    tempVariable = ""
                    for v in range(4, len(k)):
                        tempVariable += k[v] + " "
                        tempList.append(tempVariable)
            list[list.index(k)] = tempList
            return list

space = "                                                                                                                                                                                                              "
