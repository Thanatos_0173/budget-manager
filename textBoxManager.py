import tkinter as tk

def textBox(window):
    global logBox
    logBox = tk.Text(
        window,
        width = 223,
        height = 7
    )

    logBox.grid(row = 3,column=0,padx=30,columnspan=3,rowspan=2)
    logBox.config(state = tk.DISABLED)
    sb = tk.Scrollbar(tk.Frame(window))
    sb.pack(side=tk.RIGHT, fill=tk.BOTH)
    logBox.config(yscrollcommand=sb.set)
    sb.config(command=logBox.yview)


def addMessage(message):
    logBox.config(state = tk.NORMAL)
    logBox.insert('end',message+"\n")
    logBox.config(state = tk.DISABLED)

