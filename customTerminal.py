import tkinter as tk
import commands


window = tk.Tk()

def addMessage(message):
    logBox.insert('end',message)
    logBox.tag_config("color_tag")


logBox = tk.Text(
    window
)
logBox.pack()
sb = tk.Scrollbar(tk.Frame(window))
sb.pack(side=tk.RIGHT, fill=tk.BOTH)
logBox.config(yscrollcommand=sb.set)
sb.config(command=logBox.yview)
logBox.config(background="black",foreground="white")
addMessage(">> ")






def getCommandInputed():
    return (logBox.get(1.0,"end-1c").split("\n")[-1])[3:]

def check_focus_return(event):
    if event.widget == logBox and event.keysym == 'Return':
        if(commands.commandExist(getCommandInputed().split(" ")[0])):
            addMessage(commands.sendCommand(getCommandInputed().split(" ")[0],
                                            getCommandInputed().split(" ")[1:])

        )



        addMessage("\n>> ")
        return "break"
    if event.keysym == 'BackSpace' and getCommandInputed() == '':
        return "break"


logBox.bind("<Key>", check_focus_return)


window.mainloop()
