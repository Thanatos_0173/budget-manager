import tkinter as tk
import menuScreen as ms
import customFileSaver as cfs
import textBoxManager as tbm
from publicVariables import getValues

def save(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,rootWindow):
    isFileSaved = cfs.save(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,rootWindow)
    if isFileSaved : tbm.addMessage("File "+ getValues(UUID).get('name') +" saved")

def saveAs(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,rootWindow):
    isFileSaved = cfs.save(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,rootWindow,True)
    if isFileSaved : tbm.addMessage("File "+ getValues(UUID).get('name') +" saved")



def menuButton(window,moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID):
    mainmenu = tk.Menu(window)
    mainmenu.add_command(label = "Menu", command= ms.menu)

    window.config(menu = mainmenu)

    file_menu = tk.Menu(mainmenu,tearoff = False)

    file_menu.add_command(label="Save As...",
                          command=lambda:saveAs(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,window))

    file_menu.add_command(label="Save",
                          command=lambda:save(moneyClass,moneyQuantity,cash,actionHappened,defaultValues,UUID,window))
    mainmenu.add_cascade(label="File",menu = file_menu)
    window.config(menu = mainmenu)
