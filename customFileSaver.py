import os
import cryption
from tkinter import filedialog

import publicVariables as pv

def loadOrCreateDirectory(path, dirToCheckOrCreate):
    os.chdir(path)
    customDir = path +"/"+ dirToCheckOrCreate
    if not os.path.exists(customDir):
        os.makedirs(customDir)


def loadOrCreateFile(path,fileToCheckOrCreate):
    os.chdir(path)
    customdir = path + "/" + fileToCheckOrCreate
    if not os.path.exists(customdir):
        fp = open(fileToCheckOrCreate, 'x')
        fp.close()

def getContent(path,filename):
    try:
        os.chdir(path)
        with open(filename + '.th','r') as content:
            dict =  cryption.decrypt(content.read())
            return [dict.get("money").get("moneyClass"),dict.get("money").get("moneyQuantity"),dict.get("removedMoney"),dict.get("money").get("cash")]
    except OSError:
        None

def save(moneyClass,moneyQuantity,cash,removedMoney,defaultMoney,UUID,window,*forceAlreadyExist):
    if(len(forceAlreadyExist) > 1 or type(forceAlreadyExist[0]) != bool): raise ValueError("forceAlreadyExist argument must be a single bool")
    data = {"money": {
        "moneyClass":moneyClass,
	    "moneyQuantity":moneyQuantity,
        "defaultMoney":defaultMoney,
        "cash":cash},
    "removedMoney": removedMoney}
    if pv.getValues(UUID).get('isUntitled') or forceAlreadyExist[0]:
        types  = [('Th files','*.th')]
        f = filedialog.asksaveasfile(mode='w',defaultextension=".th",filetypes=types)
        if f is None:
            return False
        if f:
            name = os.path.basename(f.name).split('.')[0]
            path = os.path.dirname(f.name)
            f.close()
            with open(name+".th",'w') as wrote:
                wrote.seek(0)
                wrote.write(str(cryption.encrypt(data)))
                wrote.truncate()
            window.title(name)
            os.chdir(path)
            with open(name+'.th',"w") as wrote:
                wrote.seek(0)
                wrote.write(str(cryption.encrypt(data)))
                wrote.truncate()
            pv.changeValues(UUID = UUID,name = name, path = path, isUntitled = False)
            return True
    else:
        os.chdir((pv.getValues(UUID).get('path')))
        with open(pv.getValues(UUID).get('name')+'.th',"w") as wrote:
            wrote.seek(0)
            wrote.write(str(cryption.encrypt(data)))
            wrote.truncate()
        return True
