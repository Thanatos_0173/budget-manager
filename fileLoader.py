import customFileSaver as cfs
import json
import os

def loadDict(dict,isEntitled):
    if isEntitled:
        return [[],[],[],[],0]
    return [dict.get("money").get("moneyClass"),dict.get("money").get("moneyQuantity"),dict.get("removedMoney"),dict.get("money").get("defaultMoney"),dict.get("money").get("cash")]
