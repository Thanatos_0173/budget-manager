import tkinter as tk
import miscAdditions as ma
import main as m
import os
import cryption

import publicVariables as pv

from tkinter import filedialog as fd
from tkinter import ttk
def menu():
    global rootWindow
    rootWindow = tk.Tk()
    rootWindow.resizable(True, True)
    rootWindow.title("budgetGestionnary.th")

    def openFile():
        filetypes = [('th files', '*.th')]
        filename = fd.askopenfilename(
            title='Open a file',
            filetypes=filetypes
        )
        try:
            with open(filename,'r') as content:
                rootWindow.destroy()
                uuid = pv.addValues(name = os.path.splitext(os.path.basename(filename))[0], path = os.path.dirname(filename),isUntitled = False)
                m.main(cryption.decrypt(content.read()), uuid)
        except TypeError:
            pass


    def createFile():
        rootWindow.destroy()
        uuid = pv.generateValues(True)
        m.main({},uuid)

    #Buttons
    text = tk.Label(rootWindow,text=" Budget Manager")
    text.grid(row = 1,column = 1)

    
    create = tk.Button(rootWindow,text="Create",command=createFile)
    create.grid(row=1,column=0)


    opene = tk.Button(rootWindow,text="Open",command = openFile)
    opene.grid(row = 2, column = 0)


    rootWindow.mainloop()
