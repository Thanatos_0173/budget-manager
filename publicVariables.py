
currentData = []

class ANSI():
	def background(code):
		return "\33[{code}m".format(code=code)

	def style_text(code):
		return "\33[{code}m".format(code=code)

	def color_text(code):
		return "\33[{code}m".format(code=code)


def addValues(name : str ,path : str ,isUntitled : bool):
    currentData.append(
        {
        'name' : name,
        'path' : path,
        'isUntitled' : isUntitled
        }
    )
    return len(currentData) - 1 #Cause we added a value

def getValues(UUID):
    try:
        return {
            'name' : currentData[UUID].get('name'),
            'path' : currentData[UUID].get('path'),
            'isUntitled' : currentData[UUID].get('isUntitled')
        }
    except IndexError:
        print('\033[91m' + "Error : UUID specified is invalid." + '\033[0m')
        return {
            'name' : '',
            'path' : '',
            'isUntitled' : False
        }
def generateValues(isUntitled : bool):
    currentData.append(
        {
        'name' : None,
        'path' : None,
        'isUntitled' : isUntitled
        }
    )
    return len(currentData) - 1 #Cause we added a value as well


def changeValues(UUID, name = None, path = None, isUntitled = None):
    try:
        if name != None:
            currentData[UUID]['name'] = name
        if path != None:
            currentData[UUID]['path'] = path
        if isUntitled != None:
            currentData[UUID]['isUntitled'] = isUntitled
    except IndexError:
        print('\033[91m' + "Error : UUID specified is invalid." + '\033[0m')

'''
[
    [UUID,
    {
    'name' = name
    'path':path,
    'isUntitled':isUntitled
    }
    ]
]
'''
