import tkinter as tk
import textBoxManager as tbm
import moneyTable as mt
import moneyActionTable as mat
import cashMoney as cm

import datetime

class NotEnoughMoney(Exception):
     pass
class CantConvert(Exception):
     pass

def showOrHideButton(button,action):
     if action == "Convert money to cash":
          button.grid_forget()
          dropdown.config(state=tk.NORMAL)
     else:
          if bool(doToCash.get()): dropdown.config(state=tk.DISABLED)
          else:dropdown.config(state=tk.NORMAL)
          button.grid(column=1,row=0,sticky=tk.E,padx=10)

def loadWindow(moneyClass,moneyQuantity):
     newWindow = tk.Toplevel()
     newWindow.attributes('-topmost', True)
     newWindow.grab_set()
     newWindow.minsize(200,200)
     newWindow.resizable(False,False)
     actionStrVar= tk.StringVar()
     actionStrVar.set("Add money")
     clicked3 = tk.StringVar()
     clicked3.set(moneyClass[0])
     global dropdown
     dropdown = tk.OptionMenu(newWindow, clicked3, *moneyClass)
     dropdown.place(x=10,y=50)
     stateList = [tk.NORMAL,tk.DISABLED]

     global doToCash
     doToCash = tk.IntVar()
     chk = tk.Checkbutton(newWindow,text = "To cash",variable=doToCash,command=lambda:dropdown.config(state=stateList[doToCash.get()])) #Ignore the error at this line, the code still working perfectly.
     chk.grid(column=1,row = 0,sticky=tk.E,padx=10)
     actionDropdown = tk.OptionMenu(newWindow,actionStrVar,*["Add money","Remove money","Convert money to cash"],command=lambda e:showOrHideButton(chk,actionStrVar.get()))
     actionDropdown.grid(row=0,column=0,padx=10)
     clicked = tk.StringVar()
     clicked.set('Quantity')
     quantityEntry = tk.Entry(newWindow,textvariable=clicked)
     quantityEntry.place(x=10,y=110)
     clicked2 = tk.StringVar()
     clicked2.set('Cause')
     causeEntry = tk.Entry(newWindow,textvariable=clicked2)
     causeEntry.place(x=10,y=85)
     def action():
          sign = "-"
          text = "removed"
          addOrRemove = -1
          if actionStrVar.get() == "Add money":
              addOrRemove = 1
              sign = "+"
              text = "added"
          if actionStrVar.get() == "Convert money to cash":
              sign = "<->"
              text = "transfered to cash"
          newWindow.destroy()
          c = clicked.get()
          try:
               category = "cash" if bool(doToCash.get()) and actionStrVar.get() != "Convert money to cash" else moneyClass.index(clicked3.get())
               try:
                    if bool(doToCash.get()) and actionStrVar.get() != "Convert money to cash" :cm.addMoney(float(c)*addOrRemove)
                    else: moneyQuantity[category] += float(c) * addOrRemove
               except:
                    tbm.addMessage('>> Impossible to convert "' + c + '" to a number')
                    raise CantConvert
               if bool(doToCash.get()) and actionStrVar.get() != "Convert money to cash":
                    clicked3.set(category)
                    if cm.returnValue() < 0:
                         cm.addMoney(float(c))
                         tbm.addMessage('>> Impossible to remove ' + c + ' € from "' + clicked3.get() + '" category: you haven\'t enough money.')
                         raise NotEnoughMoney
               else:
                    if moneyQuantity[category] < 0:
                         moneyQuantity[category] += float(c)
                         tbm.addMessage('>> Impossible to remove ' + c + ' € from "' + clicked3.get() + '" category: you haven\'t enough money.')
                         raise NotEnoughMoney
                    mt.changeValue(clicked3.get(),moneyQuantity[category])
               mat.addElement("[" + sign + "]",datetime.datetime.today().strftime('%d/%m/%Y'),clicked3.get(),c,clicked2.get())
               tbm.addMessage('>> Sucessfully ' + text +' '+ c + ' € from "' + clicked3.get() + '" category')
               if actionStrVar.get() == "Convert money to cash":
                    cm.addMoney(float(c))
          except (NotEnoughMoney,CantConvert): pass


     button = tk.Button(newWindow,text = "Validate",command = action)
     button.place(x=10,y=150)
