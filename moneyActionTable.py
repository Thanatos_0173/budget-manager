import tkinter as tk
from tkinter import ttk
import miscAdditions as ma

def resizeColumn():
    for column in column_list:
        tree.heading(column, text=column)
        max = 0
        for i in range(len(elementList)):
            if len(elementList[i]) > max:
                max = len(elementList[i])
        if column == column_list[0]:
            tree.column(column, width = 50)
        elif column == column_list[2] or column == column_list[4]:
            tree.column(column, width=max*50)
        else:
            tree.column(column, width=65)

def moneyActionTable(window,list:list):
    frame = tk.LabelFrame(window)
    frame.grid(padx=30, pady=10,sticky=tk.W, ipadx = 300,ipady=350, row = 0,column=1)
    frame.config(background=(ma.rgb_to_hex(100, 100, 100)),bd=3)
    global tree
    tree = ttk.Treeview(frame)
    global elementList
    elementList = list
    global column_list
    column_list = [" ","Date","Category", "Money","Cause"]
    tree['columns'] = column_list
    tree["show"] = "headings"
    resizeColumn()
    tree.place(relheight=1, relwidth=1)
    treescrollY = tk.Scrollbar(frame)
    treescrollY.configure(command=tree.yview)
    tree.configure(yscrollcommand=treescrollY.set)
    treescrollY.pack(side="right", fill="y")
    treescrollX = tk.Scrollbar(frame, orient=tk.HORIZONTAL)
    treescrollX.configure(command=tree.xview)
    tree.configure(xscrollcommand=treescrollX.set)
    treescrollX.pack(side="bottom", fill="x")

    def handle_click(event):
        if tree.identify_region(event.x, event.y) == "separator":
            return "break"
    tree.bind('<Button-1>', handle_click)
    ma.reloadTreeview(tree, elementList)
    return tree

def addElement(categorized:str,date:str,category:str,money:str,cause:str):
    elementList.append([categorized,date,category,money,cause])
    resizeColumn()
    ma.reloadTreeview(tree,elementList)





